package nl.bioinf.sarends.readmissionreader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class edits an existing arff file.
 *
 * @author Stijn Arends
 * @version 1.0
 */
public class EditArffFile {

    private final String filePath = "application_files/empty_arff_file.arff";
    private final String inputInstance;

    /**
     * Sets the inputInstance.
     *
     * @param inputInstance a string containing data for one instance
     */
    public EditArffFile(String inputInstance) {
        this.inputInstance = inputInstance;
    }

    /**
     * Calls different methods and calls a method from another class.
     *
     * <p>
     *     Check in the {@link FIleParser#checkInstance(String)} method
     *     if the instance has the right amount of attributes.
     * </p>
     * <p>
     *     After that write the {@link #inputInstance} to a pre made arff file
     *     in the {@link #writeToArffFile(String)} method.
     * </p>
     * <p>
     *     Then call the {@link FIleParser#start()} method.
     * </p>
     * <p>
     *     Finally removes the {@link #inputInstance} line from the
     *     created arff file from the {@link #writeToArffFile(String)} method
     *     in the {@link #removeLine(String)} method.
     * </p>
     *
     * @see FIleParser#checkInstance(String) 
     * @see #writeToArffFile(String)
     * @see FIleParser#start()
     * @see #removeLine(String)
     */
    public void start(){
        try{
            FIleParser fIleParser = new FIleParser(this.filePath);
            if (fIleParser.checkInstance(this.inputInstance) == 19){
                writeToArffFile(this.inputInstance);
                fIleParser.start();
                removeLine(this.inputInstance);
            } else {
                System.out.println("The instance doesn't have the right amount of attributes.");
            }
        } catch (Exception e){
            System.err.println("An error occurred");
            System.err.println("Available information: " + e.getMessage());
        }
    }

    /**
     * Writes the inputInstance string to an existing arff file.
     *
     * @param inputInstance a string of the instance given on the commandline
     */
    public void writeToArffFile(String inputInstance){
        try {
            Files.write(Paths.get("/homes/sarends/jaar_3/Thema_9/javawrapper/build/libs/application_files/empty_arff_file.arff"), inputInstance.getBytes(), StandardOpenOption.APPEND);
        }catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
    }

    /**
     * Removes the string inputInstance from the arff file.
     *
     * @param lineContent a string containing the instance given on the command line.
     * @throws IOException
     */
    public void removeLine(String lineContent) throws IOException
    {
        File file = new File(this.filePath);
        List<String> out = Files.lines(file.toPath())
                .filter(line -> !line.contains(lineContent))
                .collect(Collectors.toList());
        Files.write(file.toPath(), out, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
    }
}
