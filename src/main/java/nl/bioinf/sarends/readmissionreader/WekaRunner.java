package nl.bioinf.sarends.readmissionreader;

import weka.classifiers.Evaluation;
import weka.classifiers.meta.AdaBoostM1;
import weka.classifiers.meta.CostSensitiveClassifier;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Normalize;

import javax.swing.*;
import java.io.IOException;
import java.util.Random;

/**
 * This class predicts the class label for new instances.
 *
 * @author Stijn Arends
 * @version 1.0
 */
public class WekaRunner {

    private final String file;
    private final String modelFile = "application_files/AdaBoostM1.model";

    /**
     * Sets the file.
     *
     * @param file a string of the filepath.
     */
    public WekaRunner(String file) {
        this.file = file;
    }

    /**
     * Calls different methods.
     *
     * <p>
     *     First determine if the argument equals -i or -f. This is done by
     *     looking at the name of the file.
     * </p>
     * <p>
     *     Second in the {@link #loadClassifier()} method load in the model.
     * </p>
     * <p>
     *     Then load in the unknown data in the {@link #loadArff(String)} method.
     * </p>
     * <p>
     *     Now in the method {@link #performNormalization(Instances)} the data will
     *     be normalized. But only if the argument equals to -f.
     * </p>
     * <p>
     *     Finally classify the new instances with the model in the
     *     {@link #classifyNewInstance(CostSensitiveClassifier, Instances)} method.
     * </p>
     *
     * @see #loadClassifier()
     * @see #loadArff(String)
     * @see #performNormalization(Instances)
     * @see #classifyNewInstance(CostSensitiveClassifier, Instances)
     */
    public void start() {
        String unknownFile = file;
        try {
            if (unknownFile.endsWith("empty_arff_file.arff")){
                CostSensitiveClassifier fromFile = loadClassifier();
                Instances unknownInstances = loadArff(unknownFile);
                classifyNewInstance(fromFile, unknownInstances);
            } else {
                CostSensitiveClassifier fromFile = loadClassifier();
                Instances unknownInstances = loadArff(unknownFile);
                //Instances normalizeUnknownInstances = performNormalization(unknownInstances);
                getStatistics();
                //classifyNewInstance(fromFile, normalizeUnknownInstances);
                classifyNewInstance(fromFile, unknownInstances);
            }


        } catch (Exception e) {
            System.err.println("An error occurred");
            System.err.println("Available info: " + e.getMessage());
            System.err.println("Aborting");
            System.exit(0);

        }
    }

    /**
     * Classifies new instances based on the model.
     *
     * @param adaBoostM1 model that contains the algorithm AdaBoostM1
     * @param unknownInstances instances without a class label
     * @throws Exception
     */
    private void classifyNewInstance(CostSensitiveClassifier adaBoostM1, Instances unknownInstances) throws Exception {
        // create copy
        Instances labeled = new Instances(unknownInstances);
        // label instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = adaBoostM1.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        System.out.println("\nNew, labeled = \n" + labeled);
        //popUpWindow(labeled);
    }

    /**
     * Reads in the model.
     *
     * @return model
     * @throws Exception
     */
    private CostSensitiveClassifier loadClassifier() throws Exception {
        // deserialize model
        return (CostSensitiveClassifier) weka.core.SerializationHelper.read(modelFile);
    }

    /**
     * Load in the arff file with the unknown instances.
     *
     * @param datafile a filepath to the file with the unknown instances
     * @return data
     * @throws IOException
     */
    private Instances loadArff(String datafile) throws IOException {
        try {
            ConverterUtils.DataSource source = new ConverterUtils.DataSource(datafile);
            Instances data = source.getDataSet();
            // setting class attribute if the data format does not provide this information
            // For example, the XRFF format saves the class attribute information as well
            if (data.classIndex() == -1)
                data.setClassIndex(data.numAttributes() - 1);

            return data;
        } catch (Exception e) {
            System.exit(0);
            throw new IOException("could not read from file");

        }
    }

    /**
     * Performs normalization on the data.
     *
     * @param data data object
     * @return newData
     * @throws Exception
     */
    private Instances performNormalization(Instances data) throws Exception {
        Normalize normalize = new Normalize();
        normalize.setInputFormat(data);
        Instances newData = Filter.useFilter(data, normalize);
        return(newData);

    }

    /**
     * Gets the statistics from the model.
     *
     * @throws Exception
     */
    public void getStatistics() {
        try {
            ConverterUtils.DataSource source = new ConverterUtils.DataSource("application_files/norm_complete_HD_data_g2.arff");
            Instances dataset = source.getDataSet();
            //set class index to last attribute
            dataset.setClassIndex(dataset.numAttributes()-1);
            //create and build the classifier
            CostSensitiveClassifier costSensitiveClassifier = loadClassifier();
            costSensitiveClassifier.buildClassifier(dataset);

            Evaluation eval = new Evaluation(dataset);

            //test data for evaluation
            ConverterUtils.DataSource testSource = new ConverterUtils.DataSource("application_files/test_data.arff");
            Instances testDataset = testSource.getDataSet();
            //set class index to last attribute
            testDataset.setClassIndex(testDataset.numAttributes()-1);
            eval.crossValidateModel(costSensitiveClassifier, dataset, 10, new Random(1));

            System.out.println(eval.toSummaryString("Classification statistics: \n", false));
            System.out.println("Correct % = " + eval.pctCorrect());
            System.out.println("Incorrect % = " + eval.pctIncorrect());
            System.out.println("Unclassified % = " + eval.pctUnclassified());
            System.out.println("AUC = " + eval.areaUnderPRC(1));
            System.out.println("Kappa = " + eval.kappa());
            System.out.println("MAE = " + eval.meanAbsoluteError());
            System.out.println("RMSE = " + eval.rootMeanSquaredError());
            System.out.println("Precision = " + eval.precision(1));
            System.out.println("Recall = " + eval.recall(1));
            System.out.println("fMeasure = " + eval.fMeasure(1));
            System.out.println("Error Rate = " + eval.errorRate());

            System.out.println(eval.toClassDetailsString());

            System.out.println(eval.toMatrixString());
        } catch (Exception ex){
            System.err.println("Aborting");
            System.err.println("Available info: " + ex.getMessage());

        }


    }

    public void popUpWindow(Instances labeled){
        //google: java how to write a string to a pop up window
        //https://stackoverflow.com/questions/20341885/how-to-display-a-text-file-from-a-gui
        //google: java create a pop up text file

        JOptionPane.showMessageDialog(null,  String.valueOf(labeled), "New labeled instances", JOptionPane.INFORMATION_MESSAGE);
    }

}
