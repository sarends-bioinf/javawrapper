package nl.bioinf.sarends.readmissionreader;

import weka.gui.beans.UserRequestAcceptor;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This class parses a input file and checks if the file
 * consists of the right data.
 *
 * @author Stijn Arends
 * @version 1.0
 */
public class FIleParser {

    private final String file;
    private List<String> data = new ArrayList<>();
    private List<String> dataValues = new ArrayList<>();

    /**
     * Sets the file.
     *
     * @param file arff file that was read from the commandline.
     */
    public FIleParser(String file) {
        this.file = file;
    }

    /**
     * Calls different methods and calls a method from another class.
     * <p>
     *     First checks if the input file has the right extension in
     *     the {@link #isLegalFile(String)} method.
     * </p>
     * <p>
     *    Then it collects the attributes from the input file in the
     *    {@link #getAttributesFromFile(String)} method.
     * </p>
     * <p>
     *     Then it checks if the header of the input file corresponds to
     *     the expected header in the {@link #compareAttributes(List, List)} method.
     *</p>
     * <p>
     *     Finally it calls the {@link nl.bioinf.sarends.readmissionreader.WekaRunner}
     *     class.
     * </p>
     *
     * @see #isLegalFile(String)
     * @see #getAttributesFromFile(String)
     * @see #compareAttributes(List, List)
     * @see nl.bioinf.sarends.readmissionreader.WekaRunner
     */
    public void start(){
        try {
            if (isLegalFile(this.file)) {
                BufferedReader inputFile = getFile();
                readFile(inputFile);
                List<String> userInput = getAttributesFromFile(this.file);
                List<String> expectedInput = getAttributesFromFile("application_files/empty_arff_file.arff");

                if (compareAttributes(userInput,expectedInput)) {
                    WekaRunner wekaRunner = new WekaRunner(this.file);
                    wekaRunner.start();
                } else {
                    System.out.println("The amount of attributes doesn't correspond tot the required amount of attributes.");
                }

            } else {
                System.out.println("The file doesn't have the right extension");
            }
        }catch (Exception e){
            System.err.println("An error occurred");
            System.err.println("Available info: " + e.getMessage());
        }
    }
    /**
     * Checks for the right extension of the input file.
     *
     * @param inputFile a path to the input file
     * @return boolean
     */
    private boolean isLegalFile(String inputFile) {
        if (inputFile.endsWith(".arff")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Creates a file object from the input filepath.
     *
     * @return breader - a file object
     * @throws FileNotFoundException
     */
    public BufferedReader getFile() throws FileNotFoundException {
        try {
            File inFile = new File(this.file);

            BufferedReader breader = null;

            breader = new BufferedReader(new FileReader(inFile));

            return breader;
        } catch (FileNotFoundException e) {
            System.err.println("It looks like that the file cannot be opened. Please check if the file is not corrupted.");
            throw new FileNotFoundException("Something went wrong");
        }

    }

    /**
     * Reads the created file object and puts data in a list.
     *
     * @param breader a file object
     */

    public void readFile(BufferedReader breader) {
        try {
            String sCurrentline;
            while ((sCurrentline = breader.readLine()) != null) {
                data.add(sCurrentline);
                if (!sCurrentline.startsWith("@")){
                    dataValues.add(sCurrentline);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (breader != null) breader.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Checks if the input file has the correct amount of attributes.
     *
     * @return countAttribute
     */
    public int checkAttribute(){
        int countAttribute = 0;
        //System.out.println(this.data);

        for (String line: this.data){
            if (line.startsWith("@attribute")){
                countAttribute += 1;
            }
        }
        return countAttribute;
    }

    /**
     * Reads in the file and collects the attributes from that file.
     *
     * @param pathFile string of the filepath
     * @return attributeList - list of all the attributes
     */
    public List<String> getAttributesFromFile(String pathFile){
        try {
            List<String> attributeList = new ArrayList<>();

            File inFile = new File(pathFile);

            BufferedReader breader = null;

            breader = new BufferedReader(new FileReader(inFile));

            String sCurrentline;

            while ((sCurrentline = breader.readLine()) != null) {
                if (sCurrentline.startsWith("@attribute")){
                    attributeList.add(sCurrentline);
                }

            }
            return attributeList;

        } catch (IOException e) {
            System.err.println("An error occured");
            System.err.println("Available info: " + e.getMessage());
            System.err.println("Aborting");
            return null;
        }

    }

    /**
     * Compares the attributes from the input file with the expected attributes.
     *
     * @param userInput a list containing the attributes from the input file
     * @param expectedInput a list containing the expected attributes.
     * @return boolean
     */
    public boolean compareAttributes(List<String> userInput, List<String> expectedInput){
        return userInput.equals(expectedInput);
    }

    /**
     * Checks the instance for the right amount of attributes.
     *
     * @param inputInstance string of the instance
     * @return int countAttributeInstance
     */
    public int checkInstance(String inputInstance){
        String[] instanceList = inputInstance.split(",");
        int countAttributeInstances = 0;
        for (String item: instanceList){
            countAttributeInstances += 1;
        }
        return countAttributeInstances;
    }
}