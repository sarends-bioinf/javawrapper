package nl.bioinf.sarends.readmissionreader;

import org.apache.commons.cli.*;
import weka.knowledgeflow.JSONFlowUtils;

import java.awt.font.NumericShaper;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * This class parses the command line.
 *
 * @author Stijn Arends
 * @version 1.0
 */
public class ApacheCliArgumentsReader{
    private final String filePath = "application_files/empty_arff_file.arff";

    private static final String HELP = "help";
    private static final String FILE = "file";
    private static final String INSTANCE = "instance";

//    private final String[] clArguments;
    private Options options;
    private CommandLine commandLine;

    /**
     * Sets the arguments.
     * <p>
     *     Calls the {@link #initialize()} method.
     * </p>
     *
     * @see #initialize()
     */
    public ApacheCliArgumentsReader() {
//        this.clArguments = args;
        initialize();
    }


    public boolean helpRequested(){ return this.commandLine.hasOption(HELP);}

    /**
     * Initializes the buildOptions and processCommandLine methods.
     *
     * @see #buildOptions()
     * @see #initialize()
     */
    private void initialize(){
        buildOptions();
//        processCommandLine();
    }

    /**
     * Creates the buildOptions.
     * <p>
     *     Adds the option "help" to the options.
     * </p>
     * <p>
     *     Adds the option "file" to the options.
     * </p>
     * <p>
     *     Adds the option "instance" to the options.
     * </p>
     */
    private void buildOptions(){
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "The help option: prints the help message");
        Option fileOption = new Option("f", FILE, true, "File name that is going to be used in the wekarunner");
        Option instanceOption = new Option("i", INSTANCE,true, "A instance containing all attributes");

        options.addOption(helpOption);
        options.addOption(fileOption);
        options.addOption(instanceOption);

    }

    /**
     * Processes the command line.
     * <p>
     *     Checks which argument was given as input:
     * <p><ul>
     * <li> Help
     * <li> File
     * <li> Instance
     * </ul><p>
     *
     * <p>
     *     The "Help" argument prints every option and its description.
     * </p>
     * <p>
     *     The "File" argument calls a reaction of methods that
     *     check the file in the {@link nl.bioinf.sarends.readmissionreader.FIleParser} class
     *     and finally send the file to the
     *     {@link nl.bioinf.sarends.readmissionreader.WekaRunner} class.
     * </p>
     * <p>
     *     The "Instance" argument calls the class {@link nl.bioinf.sarends.readmissionreader.EditArffFile}
     *     that creates an arff file from the given input. After this the file is processed and checked in the
     *     {@link nl.bioinf.sarends.readmissionreader.FIleParser} class and finally send to the
     *     {@link nl.bioinf.sarends.readmissionreader.WekaRunner} class.
     * </p>
     *
     * @see nl.bioinf.sarends.readmissionreader.FIleParser
     * @see nl.bioinf.sarends.readmissionreader.WekaRunner
     * @see nl.bioinf.sarends.readmissionreader.EditArffFile
     *
     */
    public void processCommandLine(String[] args){
        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, args);

            if (commandLine.hasOption(HELP)){
                printHelp();
            }

            else if (commandLine.hasOption(FILE)){
                String inputFile = this.commandLine.getOptionValue(FILE);
                FIleParser fileParse = new FIleParser(inputFile);
                fileParse.start();
            }
            else if (commandLine.hasOption(INSTANCE)){
                String inputInstance = this.commandLine.getOptionValue(INSTANCE);
                EditArffFile editArffFile = new EditArffFile(inputInstance);
                editArffFile.start();
            } else {
                System.out.println("Please provide a argument:");
                printHelp();
            }


        } catch (ParseException ex) {
            throw new IllegalArgumentException(ex);
        }

    }

    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("CommandLineArgRunner", options);
    }
}

