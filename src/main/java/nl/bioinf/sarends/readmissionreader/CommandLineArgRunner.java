package nl.bioinf.sarends.readmissionreader;

//import org.apache.commons.cli.CommandLine;

public class CommandLineArgRunner {

    public static void main(String[] args) {
        ApacheCliArgumentsReader op = new ApacheCliArgumentsReader();
        try{
            op.processCommandLine(args);
        } catch(IllegalArgumentException e){
            System.err.println("Aborting");
            System.err.println("There went something wrong with the processing of the command line");
            System.err.println("Available info: " + e.getMessage());
            op.printHelp();

        }



    }

}
