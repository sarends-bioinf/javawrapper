package nl.bioinf.sarends.readmissionreader;

public interface OptionsProvider {
    /**
     * serves the name of the application user.
     * @return userName the user name
     */
    String getFile();
    /**
     * serves the verbosity level to be used.
     * @return verbosityLevel the verbosity level
     */
    String getInstance();

}
